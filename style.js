function myFunction(category) {
    var txt = "This are the Cities which come under " + category + ":<br><br>";

    var cities = {
        Mumbai : {
            id : 1,
            name: "Mumbai",
            category: ["Category-1", "Category-2"]
        }, 
        
        Delhi : {
            id: 2,
            name: "Delhi",
            category: ["Category-4", "Category-5"]
        },

        Banglore : {
            id: 3,
            name: "Banglore",
            category: ["Category-3"]
        },

        Hyderabad : {
            id : 4,
            name: "Hyderabad",
            category: ["Category-1", "Category-2"]
        }, 
        
        Ahmedabad : {
            id: 5,
            name: "Ahmedabad",
            category: ["Category-4", "Category-5"]
        },

        Chennai : {
            id: 6,
            name: "Chennai",
            category: ["Category-3"]
        },

        Kolkata : {
            id : 7,
            name: "Kolkata",
            category: ["Category-1", "Category-2"]
        }, 
        
        Surat : {
            id: 8,
            name: "Surat",
            category: ["Category-4", "Category-5"]
        },

        Pune : {
            id: 9,
            name: "Pune",
            category: ["Category-3"]
        },

        Jaipur : {
            id : 10,
            name: "Jaipur",
            category: ["Category-1", "Category-2"]
        }, 
        
        Lucknow : {
            id: 11,
            name: "Lucknow",
            category: ["Category-4", "Category-5"]
        },

        Kanpur : {
            id: 12,
            name: "Kanpur",
            category: ["Category-3"]
        },

        Nagpur : {
            id : 13,
            name: "Nagpur",
            category: ["Category-1", "Category2"]
        }, 
        
        Indore : {
            id: 14,
            name: "Indore",
            category: ["Category-4", "Category-5"]
        },

        Thane : {
            id: 15,
            name: "Thane",
            category: ["Category-3"]
        },

        Bhopal : {
            id : 16,
            name: "Bhopal",
            category: ["Category-1", "Category-2"]
        }, 
        
        Vishakapatnam : {
            id: 17,
            name: "Vishakapatnam",
            category: ["Category-4", "Category-5"]
        },

        PimpriChichwad : {
            id: 18,
            name: "Pimpri-Chinchwad",
            category: ["Category-3"]
        },

        Patna : {
            id : 19,
            name: "Patna",
            category: ["Category-1", "Category-2"]
        }, 
        
        Vadodara : {
            id: 20,
            name: "Vadodara",
            category: ["Category-4", "Category-5"]
        },

        Ghaziabad : {
            id: 21,
            name: "Ghaziabad",
            category: ["Category-3"]
        },

        Ludhiana : {
            id : 22,
            name: "Ludhiana",
            category: ["Category-1", "Category-2"]
        }, 
        
        Agra : {
            id: 23,
            name: "Agra",
            category: ["Category-4", "Category-5"]
        },

        Nashik : {
            id: 24,
            name: "Nashik",
            category: ["Category-3"]
        },

        Ranchi : {
            id : 25,
            name: "Ranchi",
            category: ["Category-1", "Category-2"]
        }, 
        
        Faridabad : {
            id: 26,
            name: "Faridabad",
            category: ["Category-4", "Category-5"]
        },

        Meerut : {
            id: 27,
            name: "Meerut",
            category: ["Category-3"]
        },

        Rajkot : {
            id : 28,
            name: "Rajkot",
            category: ["Category-1", "Category-2"]
        }, 
        
        Howrah : {
            id: 29,
            name: "Howrah",
            category: ["Category-4", "Category-5"]
        },

        Varanasi : {
            id: 30,
            name: "Varanasi",
            category: ["Category-3"]
        },

        Srinagar : {
            id : 31,
            name: "Srinagar",
            category: ["Category-1", "Category-2"]
        }, 
        
        Aurangabad : {
            id: 32,
            name: "Aurangabad",
            category: ["Category-4", "Category-5"]
        },

        Dhanbad : {
            id: 33,
            name: "Dhanbad",
            category: ["Category-3"]
        },

        Allahabad : {
            id : 34,
            name: "Allahabad",
            category: ["Category-1", "Category-2"]
        }, 
        
        Amritsar : {
            id: 35,
            name: "Amritsar",
            category: ["Category-4", "Category-5"]
        },

        Jodhpur : {
            id: 36,
            name: "Jodhpur",
            category: ["Category-3"]
        },

        Jabalpur : {
            id: 37,
            name: "Jabalpur",
            category: ["Category-3"]
        },

        Chandigarh : {
            id : 38,
            name: "Chandigarh",
            category: ["Category-1", "Category-2"]
        }, 
        
        Guwahati : {
            id: 39,
            name: "Guwahati",
            category: ["Category-5"]
        },

        Solapur : {
            id: 40,
            name: "Solapur",
            category: ["Category-3"]
        }
    };

    var x;
    for (x in cities) {
        for (y of cities[x].category) {
            if(y == category){
                txt += cities[x].name + "<br>";
            }
        } 
    }

    document.getElementById("demo").innerHTML = txt;
}